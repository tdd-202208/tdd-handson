# レンタルビデオのレンタル料計算
[新装版 リファクタリング](https://www.ohmsha.co.jp/book/9784274050190/)のレンタルビデオのレシート出力をもとに、ハンズオン向けの簡略化してます。どのようにリファクタリングしていくかは、書籍を参考にしてください。


## 仕様
レシートはこんな感じ。

```
田中太郎

親指スターウォーズ: 500 円
ドラえもん: 150 円

合計: 650 円
```

ビデオの区分は三種類。

| 区分   | 料金                           |
|------|------------------------------|
| 通常   | 二日までは 300 円、それ以降は一日当たり 200 円 |
| 新作   | 一日当たり 300 円                  |
| 子供向け | 三日までは 150 円、それ以降は一日当たり 150 円 |


## クラス図
```mermaid
classDiagram

  class Customer {
    name: string
    rentals: Rental[]
    
    add(rental: Rental)
    statement()
  }
  
  class Rental {
    movie: Movie
    daysRented: number
  }
  
  class Movie {
    title: string
    priceCode: number
  }
  
  Customer *-- Rental
  Rental --> Movie
```
